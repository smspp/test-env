#!/usr/bin/env bash

VER="${1:-"4.3.1"}"
DIR="${2:-.}/netcdf-cxx"
URL="https://github.com/Unidata/netcdf-cxx4/archive/v$VER.tar.gz"

mkdir -p "$DIR"
curl -SsL "$URL" | tar -xzC "$DIR" --strip-components 1
export CPATH="/usr/include/hdf5/serial/"
cmake -S "$DIR" -B "$DIR/build" \
	-DCMAKE_BUILD_TYPE=Release \
	-DBUILD_SHARED_LIBS=ON \
	-DNCXX_ENABLE_TESTS=OFF \
	-DCMAKE_INSTALL_PREFIX=/usr/local
cmake --build "$DIR/build" --target install
rm -r "$DIR"
