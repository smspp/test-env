#!/usr/bin/env bash

VER="${1:-"1.17.6"}"
DIR="${2:-.}/clp"
URL="https://github.com/coin-or/Clp/archive/releases/$VER.tar.gz"

mkdir -p "$DIR"
curl -SsL "$URL" | tar -xzC "$DIR" --strip-components 1
CWD=$(pwd)
cd "$DIR"
./configure --prefix=/usr/local --enable-shared
make
make install
cd "$CWD"
rm -r "$DIR"
