#!/usr/bin/env bash

VER="${1:-"1.76.0"}"
DIR="${2:-.}/boost"
URL="https://boostorg.jfrog.io/artifactory/main/release/$VER/source/boost_${VER//'.'/'_'}.tar.bz2"

mkdir -p "$DIR"
curl -SsL "$URL" | tar -xjC "$DIR" --strip-components 1
CWD=$(pwd)
cd "$DIR"
./bootstrap.sh \
	--prefix=/usr/local \
	--with-libraries=chrono,log,mpi,random,serialization,system,test,thread,timer
echo "using mpi ;" >> project-config.jam
./b2 install
cd "$CWD"
rm -r "$DIR"
