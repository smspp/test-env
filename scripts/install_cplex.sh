#!/usr/bin/env bash

VER="${1:-"12.10"}"
VER=${VER//./}
BASE_DIR=${2:-"/vagrant"}
PKG="cplex_studio$VER.linux-x86-64.bin"
DIR="$BASE_DIR/ILOG/CPLEX_Studio$VER"

if [ ! -f "$DIR/cplex/include/ilcplex/cplex.h" ]; then

	if [ -f "$BASE_DIR/$PKG" ]; then
		apt-get install -y default-jre

		"$BASE_DIR/$PKG" -i silent \
			-DLICENSE_ACCEPTED=TRUE \
			-DUSER_INSTALL_DIR=$DIR

		rm -r "$DIR/opl" \
			"$DIR/python" \
			"$DIR/doc" \
			"$DIR/swidtag" \
			"$DIR/Uninstall" \
			"$DIR/README.html"
	else
		echo "CPLEX installer not found, did you put $PKG in the main directory?" >/dev/stderr
	fi
fi

mkdir -p /opt/ibm
ln -s "$BASE_DIR/ILOG" /opt/ibm/ILOG
