#!/usr/bin/env bash

VER="${1:-"2.11.4"}"
DIR="${2:-.}/coinutils"
URL="https://github.com/coin-or/CoinUtils/archive/releases/$VER.tar.gz"

mkdir -p "$DIR"
curl -SsL "$URL" | tar -xzC "$DIR" --strip-components 1
CWD=$(pwd)
cd "$DIR"
./configure --prefix=/usr/local --enable-shared
make
make install
cd "$CWD"
rm -r "$DIR"
