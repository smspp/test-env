#!/usr/bin/env bash

VER="${1:-"3.3.9"}"
DIR="${2:-.}/eigen3"
URL="https://gitlab.com/libeigen/eigen/-/archive/$VER/eigen-$VER.tar.bz2"

mkdir -p "$DIR"
curl -SsL "$URL" | tar -xjC "$DIR" --strip-components 1
cmake -S "$DIR" -B "$DIR/build" -DCMAKE_INSTALL_PREFIX=/usr/local
cmake --build "$DIR/build" --target install
rm -r "$DIR"
