# Boost
# See https://dl.bintray.com/boostorg/release/
BOOST_VER="1.75.0"

# Eigen3
# See https://gitlab.com/libeigen/eigen/-/releases
EIGEN_VER="3.3.9"

# netCDF-C
# See https://github.com/Unidata/netcdf-c/releases
NETCDF_VER="4.7.4"

# netCDF-C++
# See https://github.com/Unidata/netcdf-cxx4/releases
NETCDFCXX_VER="4.3.1"

# CPLEX
CPLEX_VER="12.10"

# SCIP
# See https://scipopt.org/index.php#news
SCIP_VER="7.0.1"

# CoinUtils
# See https://github.com/coin-or/CoinUtils/releases
COINUTILS_VER="2.11.4"

# Osi
# See https://github.com/coin-or/Osi/releases
OSI_VER="0.108.6"

# Clp
# See https://github.com/coin-or/Clp/releases
CLP_VER="1.17.6"

# StOpt
# See https://gitlab.com/stochastic-control/StOpt/-/tags
STOPT_VER="master"
