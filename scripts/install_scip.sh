#!/usr/bin/env bash

VER="${1:-"7.0.1"}"
DIR="${2:-.}/scip"

if [ "$VER" = "7.0.2" ]; then
	source /etc/os-release
	URL="https://www.scipopt.org/download/release/SCIPOptSuite-$VER-Linux-$ID.sh"
else
	URL="https://www.scipopt.org/download/release/SCIPOptSuite-$VER-Linux.sh"
fi

CWD=$(pwd)
mkdir -p "$DIR"
cd "$DIR"
curl -SsL -o scip.sh "$URL"
chmod u+x scip.sh
./scip.sh --prefix=/usr/local --exclude-subdir --skip-license
cd "$CWD"
rm -r "$DIR"
