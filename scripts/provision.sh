#!/usr/bin/env bash

# --------------------------------------------------------------------------- #
#    Provisioning script for Vagrant SMS++ test environments                  #
#                                                                             #
#                              Niccolo' Iardella                              #
#                          Operations Research Group                          #
#                         Dipartimento di Informatica                         #
#                             Universita' di Pisa                             #
# --------------------------------------------------------------------------- #

CWD=$(pwd)

# Install required packages
export DEBIAN_FRONTEND=noninteractive
sudo -E apt-get update
sudo -E apt-get install -y build-essential clang cmake curl git pkg-config

# Get the versions on the dependencies that will be built/installed
source /vagrant/scripts/versions.sh

# Install Boost
sudo -E apt-get install -y openmpi-bin
if [ ! -d "/usr/local/include/boost" ]; then
	echo "Installing Boost $BOOST_VER (required by SMS++)"
	sudo /vagrant/scripts/install_boost.sh "$BOOST_VER"
fi

# Install Eigen3
if [ ! -d "/usr/local/include/eigen3" ]; then
	echo "Installing Eigen $EIGEN_VER (required by SMS++)"
	sudo /vagrant/scripts/install_eigen3.sh "$EIGEN_VER"
fi

# Install netCDF-C
sudo -E apt-get install -y libhdf5-dev libcurl4-openssl-dev
if [ ! -f "/usr/local/include/netcdf.h" ]; then
	echo "Installing netCDF-C $NETCDF_VER (required by SMS++)"
	sudo /vagrant/scripts/install_netcdf-c.sh "$NETCDF_VER"
fi

# Install netCDF-C++
if [ ! -f "/usr/local/include/netcdf" ]; then
	echo "Installing netCDF-C++ $NETCDFCXX_VER (required by SMS++)"
	sudo /vagrant/scripts/install_netcdf-cxx.sh "$NETCDFCXX_VER"
fi

# Install SCIP
sudo -E apt-get install -y liblapack-dev libopenblas-dev libtbb-dev
if [ ! -d "/usr/local/include/scip" ]; then
	echo "Installing SCIP $SCIP_VER (optional for MILPSolver)"
	sudo /vagrant/scripts/install_scip.sh "$SCIP_VER"
fi

# Install CPLEX
if [ ! -d "/opt/ibm/ILOG" ]; then
	echo "Installing CPLEX Studio $CPLEX_VER (optional for MILPSolver and NDOFiOracle)"
	sudo /vagrant/scripts/install_cplex.sh "$CPLEX_VER"
fi

# Install CoinUtils
sudo -E apt-get install -y libbz2-dev liblapack-dev
if [ ! -f "/usr/local/include/coin/CoinUtilsConfig.h" ]; then
	echo "Installing CoinUtils $COINUTILS_VER (required by NDOFiOracle)"
	sudo /vagrant/scripts/install_coinutils.sh "$COINUTILS_VER"
fi

# Install Osi
if [ ! -f "/usr/local/include/coin/OsiConfig.h" ]; then
	echo "Installing Osi $OSI_VER (required by NDOFiOracle)"
	sudo /vagrant/scripts/install_osi.sh "$OSI_VER"
fi

# Install Clp
if [ ! -f "/usr/local/include/coin/ClpConfig.h" ]; then
	echo "Installing Clp $CLP_VER (required by NDOFiOracle)"
	sudo /vagrant/scripts/install_clp.sh "$CLP_VER"
fi

# Install StOpt
if [ ! -d "/usr/local/include/StOpt" ]; then
	echo "Installing StOpt $STOPT_VER (required by SDDPBlock)"
	sudo /vagrant/scripts/install_stopt.sh "$STOPT_VER"
fi

# Set up GitLab and GitHub ssh credentials
mkdir -p ~/.ssh

echo "Checking connection to GitLab..."
ssh-keyscan -H gitlab.com >>~/.ssh/known_hosts
ssh -T git@gitlab.com
if [[ $? -ne 0 ]]; then
	echo "Connection to GitLab failed, do you have a valid key?" >/dev/stderr
fi

echo "Checking connection to GitHub..."
ssh-keyscan -H github.com >>~/.ssh/known_hosts
ssh -T git@github.com
if [[ $? -ne 1 ]]; then
	echo "Connection to GitHub failed, do you have a valid key?" >/dev/stderr
fi

sudo sh -c 'echo "export PATH=/vagrant/scripts:\$PATH" > /etc/profile.d/add_scripts_path.sh'
sudo sh -c 'echo "complete -F _longopt smsbuild" >> /etc/profile.d/add_scripts_path.sh'
echo "Provisioning complete!"
