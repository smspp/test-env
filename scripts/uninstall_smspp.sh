#!/usr/bin/env bash

# GoogleTest stuff
sudo rm -rf /usr/local/include/gmock
sudo rm -rf /usr/local/include/gtest
sudo rm -rf /usr/local/lib/libgmock*
sudo rm -rf /usr/local/lib/libgtest*
sudo rm -rf /usr/local/lib/pkgconfig/gmock*
sudo rm -rf /usr/local/lib/pkgconfig/gtest*
sudo rm -rf /usr/local/lib/cmake/GTest

# MCFClass
sudo rm -rf /usr/local/include/MCFClass
sudo rm -rf /usr/local/lib/libMCFClass*
sudo rm -rf /usr/local/lib/cmake/MCFClass
sudo rm -rf /usr/local/share/MCFClass

# NDOFiOracle
sudo rm -rf /usr/local/include/NDOFiOracle
sudo rm -rf /usr/local/lib/libNDOFiOracle*
sudo rm -rf /usr/local/lib/cmake/NDOFiOracle
sudo rm -rf /usr/local/share/NDOFiOracle

# SMS++ headers
sudo rm -rf /usr/local/include/SMS++

# SMS++ libraries
sudo rm -rf /usr/local/lib/libSMSpp*
sudo rm -rf /usr/local/lib/libBundleSolver*
sudo rm -rf /usr/local/lib/libMCFBlock*
sudo rm -rf /usr/local/lib/libMILPSolver*
sudo rm -rf /usr/local/lib/libLukFiBlock*
sudo rm -rf /usr/local/lib/libSDDPBlock*
sudo rm -rf /usr/local/lib/libSMILPBlock*
sudo rm -rf /usr/local/lib/libStochasticBlock*
sudo rm -rf /usr/local/lib/libUCBlock*

# SMS++ CMake stuff
sudo rm -rf /usr/local/lib/cmake/SMSpp
sudo rm -rf /usr/local/lib/cmake/BundleSolver
sudo rm -rf /usr/local/lib/cmake/MCFBlock
sudo rm -rf /usr/local/lib/cmake/MILPSolver
sudo rm -rf /usr/local/lib/cmake/LukFiBlock
sudo rm -rf /usr/local/lib/cmake/SDDPBlock
sudo rm -rf /usr/local/lib/cmake/SMILPBlock
sudo rm -rf /usr/local/lib/cmake/StochasticBlock
sudo rm -rf /usr/local/lib/cmake/UCBlock

# SMS++ binaries
sudo rm -rf /usr/local/bin/dmx2nc4
sudo rm -rf /usr/local/bin/nc4generator
sudo rm -rf /usr/local/bin/thermalunit_solver
sudo rm -rf /usr/local/bin/ucblock_solver

# SMS++ documents
sudo rm -rf /usr/local/share/SMSpp
sudo rm -rf /usr/local/share/BundleSolver
sudo rm -rf /usr/local/share/MCFBlock
sudo rm -rf /usr/local/share/MILPSolver
sudo rm -rf /usr/local/share/LukFiBlock
sudo rm -rf /usr/local/share/SDDPBlock
sudo rm -rf /usr/local/share/SMILPBlock
sudo rm -rf /usr/local/share/StochasticBlock
sudo rm -rf /usr/local/share/UCBlock

# Completion scripts
sudo rm -rf /usr/local/share/bash-completion/completions/block_solver
sudo rm -rf /usr/local/share/bash-completion/completions/sddp_greedy_solver
sudo rm -rf /usr/local/share/bash-completion/completions/thermalunit_solver
sudo rm -rf /usr/local/share/bash-completion/completions/ucblock_solver
sudo rm -rf /usr/local/share/zsh/site-functions/_block_solver
sudo rm -rf /usr/local/share/zsh/site-functions/_sddp_greedy_solver
sudo rm -rf /usr/local/share/zsh/site-functions/_thermalunit_solver
sudo rm -rf /usr/local/share/zsh/site-functions/_ucblock_solver
