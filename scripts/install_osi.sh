#!/usr/bin/env bash

VER="${1:-"0.108.6"}"
DIR="${2:-.}/osi"
URL="https://github.com/coin-or/Osi/archive/releases/$VER.tar.gz"

CPLEX_DIR=`ls -bd1 /opt/ibm/ILOG/CPLEX_Studio* | tail -n1`
CPLEX_LIB_DIR=`ls -bd1 $CPLEX_DIR/cplex/lib/*/static_pic | tail -n1`

mkdir -p "$DIR"
curl -SsL "$URL" | tar -xzC "$DIR" --strip-components 1
CWD=$(pwd)
cd "$DIR"
./configure --prefix=/usr/local --enable-shared \
	--with-cplex-incdir="$CPLEX_DIR/cplex/include/ilcplex" \
	--with-cplex-lib="-L$CPLEX_LIB_DIR -lcplex -lpthread -lm -ldl"
make
make install
cd "$CWD"
rm -r "$DIR"
