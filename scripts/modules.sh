unset modules
unset dirs
unset urls
declare -a modules
declare -a dirs
declare -a urls

umbrella_url="git@gitlab.com:smspp/smspp-project.git"

# Requirements

modules+=("MCFClass")
dirs+=("MCFClass")
urls+=("git@github.com:frangio68/Min-Cost-Flow-Class.git")

modules+=("NDOFiOracle")
dirs+=("BundleSolver/NdoFiOracle")
urls+=("git@gitlab.com:frangio68/ndosolver_fioracle_project.git")

# Core library

modules+=("SMS++")
dirs+=("SMS++")
urls+=("git@gitlab.com:smspp/smspp.git")

# Blocks

modules+=("UCBlock")
dirs+=("UCBlock")
urls+=("git@gitlab.com:smspp/ucblock.git")

modules+=("MCFBlock")
dirs+=("MCFBlock")
urls+=("git@gitlab.com:smspp/mcfblock.git")

modules+=("MMCFBlock")
dirs+=("MMCFBlock")
urls+=("git@gitlab.com:smspp/mmcfblock.git")

modules+=("LukFiBlock")
dirs+=("LukFiBlock")
urls+=("git@gitlab.com:smspp/lukfiblock.git")

modules+=("StochasticBlock")
dirs+=("StochasticBlock")
urls+=("git@gitlab.com:smspp/stochasticblock.git")

modules+=("SDDPBlock")
dirs+=("SDDPBlock")
urls+=("git@gitlab.com:smspp/sddpblock.git")

# Solvers

modules+=("MILPSolver")
dirs+=("MILPSolver")
urls+=("git@gitlab.com:smspp/milpsolver.git")

modules+=("BundleSolver")
dirs+=("BundleSolver")
urls+=("git@gitlab.com:smspp/bundlesolver.git")

modules+=("DPSolver")
dirs+=("DPSolver")
urls+=("git@gitlab.com:smspp/dpsolver.git")

modules+=("LagrangianDualSolver")
dirs+=("LagrangianDualSolver")
urls+=("git@gitlab.com:smspp/lagrangiandualsolver.git")

# Tools and system tests

modules+=("tools")
dirs+=("tools")
urls+=("git@gitlab.com:smspp/tools.git")

# modules+=("tests")
# dirs+=("tests")
# urls+=("git@gitlab.com:smspp/tests.git")