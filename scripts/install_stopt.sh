#!/usr/bin/env bash

VER="${1:-"master"}"
DIR="${2:-.}/stopt"
URL="https://gitlab.com/stochastic-control/StOpt/-/archive/$VER/StOpt-$VER.tar.bz2"

mkdir -p "$DIR"
curl -SsL "$URL" | tar -xjC "$DIR" --strip-components 1
cmake -S "$DIR" -B "$DIR/build" \
	-DBUILD_SHARED_LIBS=ON \
	-DBUILD_PYTHON=OFF \
	-DBUILD_TEST=OFF \
	-DCMAKE_INSTALL_PREFIX=/usr/local
cmake --build "$DIR/build" --target install
rm -r "$DIR"
