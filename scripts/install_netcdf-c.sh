#!/usr/bin/env bash

VER="${1:-"4.8.0"}"
DIR="${2:-.}/netcdf-c"
URL="https://github.com/Unidata/netcdf-c/archive/v$VER.tar.gz"

mkdir -p "$DIR"
curl -SsL "$URL" | tar -xzC "$DIR" --strip-components 1
cmake -S "$DIR" -B "$DIR/build" \
	-DCMAKE_BUILD_TYPE=Release \
	-DBUILD_SHARED_LIBS=ON \
	-DBUILD_UTILITIES=OFF \
	-DENABLE_EXAMPLES=OFF \
	-DENABLE_TESTS=OFF \
	-DENABLE_CONVERSION_WARNINGS=OFF \
	-DCMAKE_INSTALL_PREFIX=/usr/local
cmake --build "$DIR/build" --target install
rm -r "$DIR"
