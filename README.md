# SMS++ Test Environment

Vagrant environments for testing the building and the deployment of the SMS++ project.
At the moment we offer the following environments:

- `debian` - Debian 10 with Clang 7.0 and GCC 8.3.0.
- `ubuntu` - Ubuntu 20.04 LTS with Clang 10 and GCC 9.3.0.

## Getting started

These instructions will let you run a Vagrant environment for
building the SMS++ project on your local machine.

### Requirements

- [Oracle VM VirtualBox]
- [Vagrant]

#### SSH keys

> **Note:** Since SMS++ repositories are now public, this part is not required.

If you want to fetch SMS++ from private repositories,
you will need to setup SSH keys for your GitLab and GitHub accounts *on your
host machine*. See:

- [GitLab and SSH keys]
- [Connecting to GitHub with SSH]

SSH agent forwarding is enabled, so you don't need to manually provide
SSH keys to the guest machine.

#### SMS++ Requirements

The following SMS++ requirements are installed during the provisioning:

- [Boost]
- [netCDF-C/C++]
- [Eigen]
- [CoinUtils]
- [Osi]
- [Clp]
- [StOpt]
- [IBM ILOG CPLEX Optimization Studio]
- [SCIP]

If you want to specify the versions of the dependencies that will be installed,
edit [`scripts/versions.sh`](scripts/versions.sh).

#### CPLEX

Some SMS++ modules depend on [IBM ILOG CPLEX Optimization Studio];
if you want to build them you will need to provide a CPLEX Linux installation
by putting the `ILOG` directory into the main directory
(either physically or with a symbolic link).

If you place the Linux installer
(e.g. `cplex_studio1210.linux-x86-64.bin`) in the main directory the
provisioning script will extract and use it. 

### Configuration

The environment can fetch SMS++ source code from the remote repositories or
from directories in the host machine. For private remote repositories, you will
need access to them and the SSH keys properly configured (see above).

You can specify which modules to build by editing the
[`scripts/modules.sh`](scripts/modules.sh).
By default, that file contains all the modules that have been tested with
these environments.

To use your local SMS++ directory, edit the following line in the
[`Vagrantfile`](Vagrantfile):

```ruby
config.vm.synced_folder "<your/path/here>", "/home/vagrant/smspp-project"
```

> **Note:** in Vagrant, sharing of host directories goes in both ways, so if 
> you change the source code from inside the environment it will be reflected on
> the host.

## Running the environment

The following instructions are for running the environment.
To build the SMS++ project from inside them, see below.

- From the main directory, create and provision the virtual machine with:

  ```sh
  vagrant up <env-name> # Either debian or ubuntu
  ```

- After provisioning, you can access the environment with:

  ```sh
  vagrant ssh <env-name>
  ```

- To shutdown the environment run:

  ```sh
  vagrant halt <env-name>
  ```

  Shutting down the environment keeps the local changes intact.

> **Note:** if you don't halt the machine, it will keep running in background.

- To destroy the environment run:

  ```sh
  vagrant destroy <env-name>
  ```

  Doing so destroys any local changes (except the CPLEX installation), so
  next time you will need to provision the environment again.

## Building SMS++

When inside the environment, you can build SMS++ with the `smsbuild` command:

  ```sh
  smsbuild [options] <build-path>
  ```

The available options are:

| Option                | Effect                             |
| --------------------- | ---------------------------------- |
| `-v`, `--verbose`     | Print verbose output               |
| `--clang`             | Use Clang (default)                |
| `--gcc`               | Use GCC                            |
| `--type=<build-type>` | CMake build type                   |
| `--static`            | Build static libraries (default)   |
| `--shared`            | Build shared libraries             |
| `-t`, `--test`        | Build the tests                    |
| `-c`, `--clone`       | Clone from the remote repositories |
| `-u`, `--umbrella`    | Use the umbrella project           |
| `-i`, `--install`     | Install the project                |
| `--h`, `--help`       | Print the available options        |

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of
conduct, and the process for submitting merge requests to us.

## Authors

- **Niccolò Iardella**  
  *Operations Research Group*  
  Dipartimento di Informatica  
  Università di Pisa

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE)
file for details.

This license applies only to the content of this repository. In particular, it
**does not** apply to any third-party components and other SMS++ components
installed in the generated environments.

[Vagrant]: https://www.vagrantup.com
[Oracle VM VirtualBox]: https://www.virtualbox.org

[GitLab and SSH keys]: https://docs.gitlab.com/ee/ssh/
[Connecting to GitHub with SSH]: https://help.github.com/en/articles/connecting-to-github-with-ssh

[Boost]: https://www.boost.org
[netCDF-C/C++]: https://www.unidata.ucar.edu/software/netcdf
[Eigen]: http://eigen.tuxfamily.org/
[CoinUtils]: https://github.com/coin-or/CoinUtils
[Osi]: https://github.com/coin-or/Osi
[Clp]: https://github.com/coin-or/Clp
[StOpt]: https://gitlab.com/stochastic-control/StOpt
[SCIP]: https://www.scipopt.org
[IBM ILOG CPLEX Optimization Studio]: https://www.ibm.com/products/ilog-cplex-optimization-studio
